//
//  ViewController.swift
//  Practica5
//
//  Created by MTI on 04/03/19.
//  Copyright © 2019 MTI. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var movies: UITableView!
    
    //Repositorio de peliculas
    var movieList = [movieModel]()
    var selectedMovie: movieModel?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        movies.dataSource = self
        movies.delegate = self
        
        getMovies()
        movies.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movie") as! MovieTableViewCell
        cell.imageMovie.image = movieList[indexPath.row].movieImage
        cell.lblDuracion.text = movieList[indexPath.row].duracion
        cell.lblGenero.text = movieList[indexPath.row].genero
        cell.lblTitulo.text = movieList[indexPath.row].movieTitle
        return cell
    }
    //Request de peliculas
    func getMovies(){
        // La profecia
        movieList.append(movieModel(movieTitle: "La profecia", sinopsis: ", un niño aparentemente normal que es criado en el seno de una familia adinerada norteamericana. Cuando su maduro padre comience a sospechar del siniestro linaje del niño, hechos terroríficos comenzaran a suceder a la familia y a sus allegados", genero: "Terror", duracion: "120min", movieImage: UIImage(named: "omen")!))
        // Scream
        movieList.append(movieModel(movieTitle: "Scream", sinopsis: "En ella, una muchacha y sus amigos se convierten en víctimas de un serial killer que les acecha. Y es que tanto el asesino como las muertes se fundamentan en los pilares del género cinematográfico del slasher y en los tópicos de este tipo de cine. ", genero: "Terror", duracion: "130min", movieImage: UIImage(named: "scream")!))
        
        // El espinazo del diablo
        movieList.append(movieModel(movieTitle: "El espinazo del diablo", sinopsis: "El espinazo del Diablo es una de las producciones por las que el director ha demostrado su fascinación por lo sobrenatural y por la posguerra española. Marisa Paredes, Eduardo Noriega y Federico Luppi protagonizan esta cinta ambientada en un orfanato durante la Guerra Civil. ", genero: "Terror", duracion: "110min", movieImage: UIImage(named: "espina")!))
        
        // El orfanato
        movieList.append(movieModel(movieTitle: "El orfanato", sinopsis: "Belén Rueda y Fernando Cayo encarnan a un matrimonio que llegan a vivir una mansión junto a su hijo pequeño, Simón. Laura (Rueda) es una mujer que había vivido de niña en la mansión, un orfanato por entonces. Ahora que la casa está abandonada pretende reconvertirlo en un hogar para niños, sin embargo la desaparición de Simón complicará sus planes, dando paso a una pesadilla. ", genero: "Terror", duracion: "90min", movieImage: UIImage(named: "orn")!))
        
        movieList.append(movieModel(movieTitle: "Constantine", sinopsis: "Un hombre que puede ver demonios ayuda a una mujer policía escéptica a investigar la misteriosa muerte de su hermana gemela.", genero: "Terror", duracion: "2:01", movieImage: UIImage(named: "constantine")!))
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMovie = movieList[indexPath.row]
        performSegue(withIdentifier: "detalle", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detalle" {
            let vc = segue.destination as! detaileViewController
            vc.movie = selectedMovie
        }
    }
}

